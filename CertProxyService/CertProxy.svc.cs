﻿using System;
using CERTCLIENTLib;

namespace CertProxyService
{
    public class CertProxy : ICertProxy
    {
        private const string CaServerUrl = @"10.207.230.5\SERVER1-CA";

        private const int CC_DEFAULTCONFIG = 0;
        private const int CC_UIPICKCONFIG = 0x1;
        private const int CR_IN_BASE64 = 0x1;
        private const int CR_IN_FORMATANY = 0;
        private const int CR_IN_PKCS10 = 0x100;
        private const int CR_DISP_ISSUED = 0x3;
        private const int CR_DISP_UNDER_SUBMISSION = 0x5;
        private const int CR_OUT_BASE64 = 0x1;
        private const int CR_OUT_CHAIN = 0x100;

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public int RequestCertificate(string message)
        {
            return SendCertificateRequest(message);
        }

        public string DownloadCertificate(int requestId)
        {
            var objCertRequest = new CCertRequest();
            int iDisposition = objCertRequest.RetrievePending(requestId, CaServerUrl);

            if (iDisposition == CR_DISP_ISSUED)
            {
                string cert = objCertRequest.GetCertificate(CR_OUT_BASE64 | CR_OUT_CHAIN);
                return cert;
            }

            return string.Empty;
        }

        /// <summary>
        ///     将证书请求发送到证书服务
        /// </summary>
        /// <param name="message">证书请求字符串</param>
        /// <returns>证书请求ID，与证书服务上的ID一致。</returns>
        private static int SendCertificateRequest(string message)
        {
            var objCertRequest = new CCertRequest();
            int iDisposition = objCertRequest.Submit(
                CR_IN_BASE64 | CR_IN_FORMATANY,
                message,
                string.Empty,
                CaServerUrl);

            switch (iDisposition)
            {
                case CR_DISP_ISSUED:
                    Console.WriteLine("The certificate had been issued.");
                    break;
                case CR_DISP_UNDER_SUBMISSION:
                    Console.WriteLine("The certificate is still pending.");
                    break;
                default:
                    Console.WriteLine("The submission failed: " + objCertRequest.GetDispositionMessage());
                    Console.WriteLine("Last status: " + objCertRequest.GetLastStatus());
                    break;
            }

            return objCertRequest.GetRequestId();
        }
    }
}