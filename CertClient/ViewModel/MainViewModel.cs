using System;
using System.Windows;
using CertClient.CertService;
using CERTENROLLLib;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace CertClient.ViewModel
{
    /// <summary>
    ///     This class contains properties that the main View can data bind to.
    ///     <para>
    ///         Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    ///     </para>
    ///     <para>
    ///         You can also use Blend to data bind with the tool's support.
    ///     </para>
    ///     <para>
    ///         See http://www.galasoft.ch/mvvm
    ///     </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private RelayCommand _requestCertCommand;
        private RelayCommand _downloadCertCommand;
        private int _requestId;
        private string _name;
        private string _dept;

        public RelayCommand RequestCertCommand
        {
            get
            {
                return _requestCertCommand
                       ?? (_requestCertCommand = new RelayCommand(
                           SendCertRequest));
            }
        }

        public RelayCommand DownloadCertCommand
        {
            get { return _downloadCertCommand ?? (_downloadCertCommand = new RelayCommand(DownloadAndInstallCert)); }
        }

        private void DownloadAndInstallCert()
        {
            try
            {
                var cert = (new CertProxyClient()).DownloadCertificate(_requestId);
                var objEnroll = new CX509Enrollment();
                objEnroll.Initialize(X509CertificateEnrollmentContext.ContextUser);
                objEnroll.InstallResponse(
                    InstallResponseRestrictionFlags.AllowUntrustedRoot,
                    cert,
                    EncodingType.XCN_CRYPT_STRING_BASE64,
                    null);
                MessageBox.Show("证书已成功安装到电脑上。");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (Set(() => Name, ref _name, value))
                {
                    RaisePropertyChanged(() => Name);
                }
            }
        }

        public string Dept
        {
            get { return _dept; }
            set
            {
                if (Set(() => Dept, ref _dept, value))
                {
                    RaisePropertyChanged(() => Dept);
                }
            }
        }

        private void SendCertRequest()
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                MessageBox.Show("请输入姓名。");
                return;
            }

            if (string.IsNullOrWhiteSpace(Dept))
            {
                MessageBox.Show("请输入部门。");
                return;
            }

            try
            {
                string message = CreateCertRequestMessage(Name, Dept);
                var client = new CertProxyClient();
                _requestId = client.RequestCertificate(message);

                MessageBox.Show(string.Format("您的申请已成功发送，申请ID为 {0}", _requestId));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        ///     创建证书请求
        /// </summary>
        /// <returns>包含证书请求所需要的所有信息的字符串</returns>
        private static string CreateCertRequestMessage(string name, string dept)
        {
            var objCSPs = new CCspInformations();
            objCSPs.AddAvailableCsps();
            var objPrivateKey = new CX509PrivateKey();
            objPrivateKey.Length = 2048;
            objPrivateKey.KeySpec = X509KeySpec.XCN_AT_SIGNATURE;
            objPrivateKey.KeyUsage = X509PrivateKeyUsageFlags.XCN_NCRYPT_ALLOW_ALL_USAGES;
            objPrivateKey.MachineContext = false;
            objPrivateKey.ExportPolicy = X509PrivateKeyExportFlags.XCN_NCRYPT_ALLOW_EXPORT_NONE; //不允许导出私钥
            objPrivateKey.CspInformations = objCSPs;
            objPrivateKey.Create();

            var objPkcs10 = new CX509CertificateRequestPkcs10();
            objPkcs10.InitializeFromPrivateKey(
                X509CertificateEnrollmentContext.ContextUser,
                objPrivateKey,
                string.Empty);

            var objExtensionKeyUsage = new CX509ExtensionKeyUsage();
            objExtensionKeyUsage.InitializeEncode(
                X509KeyUsageFlags.XCN_CERT_DIGITAL_SIGNATURE_KEY_USAGE |
                X509KeyUsageFlags.XCN_CERT_NON_REPUDIATION_KEY_USAGE |
                X509KeyUsageFlags.XCN_CERT_KEY_ENCIPHERMENT_KEY_USAGE |
                X509KeyUsageFlags.XCN_CERT_DATA_ENCIPHERMENT_KEY_USAGE);
            objPkcs10.X509Extensions.Add((CX509Extension) objExtensionKeyUsage);

            var objObjectId = new CObjectId();
            var objObjectIds = new CObjectIds();
            var objX509ExtensionEnhancedKeyUsage = new CX509ExtensionEnhancedKeyUsage();
            objObjectId.InitializeFromValue("1.3.6.1.5.5.7.3.2"); //客户端证书 OID
            objObjectIds.Add(objObjectId);
            objX509ExtensionEnhancedKeyUsage.InitializeEncode(objObjectIds);
            objPkcs10.X509Extensions.Add((CX509Extension) objX509ExtensionEnhancedKeyUsage);

            var objDN = new CX500DistinguishedName();
            string subjectName = string.Format("CN = {0}, OU = {1}, O = {1}, L = GuangZhou, S = GuangDong, C = CN", name,
                dept);
            objDN.Encode(subjectName, X500NameFlags.XCN_CERT_NAME_STR_NONE);
            objPkcs10.Subject = objDN;

            var objEnroll = new CX509Enrollment();
            objEnroll.InitializeFromRequest(objPkcs10);
            string strRequest = objEnroll.CreateRequest(EncodingType.XCN_CRYPT_STRING_BASE64);

            return strRequest;
        }
    }
}