﻿using System;
using CERTCLIENTLib;
using CERTENROLLLib;

namespace CertComTest
{
    internal class Program
    {
        private const int CC_DEFAULTCONFIG = 0;
        private const int CC_UIPICKCONFIG = 0x1;
        private const int CR_IN_BASE64 = 0x1;
        private const int CR_IN_FORMATANY = 0;
        private const int CR_IN_PKCS10 = 0x100;
        private const int CR_DISP_ISSUED = 0x3;
        private const int CR_DISP_UNDER_SUBMISSION = 0x5;
        private const int CR_OUT_BASE64 = 0x1;
        private const int CR_OUT_CHAIN = 0x100;

        /// <summary>
        /// 创建证书请求
        /// </summary>
        /// <returns>包含证书请求所需要的所有信息的字符串</returns>
        private static string CreateCertRequestMessage()
        {
            var objCSPs = new CCspInformations();
            objCSPs.AddAvailableCsps();
            var objPrivateKey = new CX509PrivateKey();
            objPrivateKey.Length = 2048;
            objPrivateKey.KeySpec = X509KeySpec.XCN_AT_SIGNATURE;
            objPrivateKey.KeyUsage = X509PrivateKeyUsageFlags.XCN_NCRYPT_ALLOW_ALL_USAGES;
            objPrivateKey.MachineContext = false;
            objPrivateKey.ExportPolicy = X509PrivateKeyExportFlags.XCN_NCRYPT_ALLOW_EXPORT_FLAG;
            objPrivateKey.CspInformations = objCSPs;
            objPrivateKey.Create();

            var objPkcs10 = new CX509CertificateRequestPkcs10();
            objPkcs10.InitializeFromPrivateKey(
                X509CertificateEnrollmentContext.ContextUser,
                objPrivateKey,
                string.Empty);

            var objExtensionKeyUsage = new CX509ExtensionKeyUsage();
            objExtensionKeyUsage.InitializeEncode(
                X509KeyUsageFlags.XCN_CERT_DIGITAL_SIGNATURE_KEY_USAGE |
                X509KeyUsageFlags.XCN_CERT_NON_REPUDIATION_KEY_USAGE |
                X509KeyUsageFlags.XCN_CERT_KEY_ENCIPHERMENT_KEY_USAGE |
                X509KeyUsageFlags.XCN_CERT_DATA_ENCIPHERMENT_KEY_USAGE);
            objPkcs10.X509Extensions.Add((CX509Extension) objExtensionKeyUsage);

            var objObjectId = new CObjectId();
            var objObjectIds = new CObjectIds();
            var objX509ExtensionEnhancedKeyUsage = new CX509ExtensionEnhancedKeyUsage();
            objObjectId.InitializeFromValue("1.3.6.1.5.5.7.3.2"); //客户端证书 OID
            objObjectIds.Add(objObjectId);
            objX509ExtensionEnhancedKeyUsage.InitializeEncode(objObjectIds);
            objPkcs10.X509Extensions.Add((CX509Extension) objX509ExtensionEnhancedKeyUsage);

            var objDN = new CX500DistinguishedName();
            string subjectName = "CN = shaunxu.me, OU = ADCS, O = Blog, L = Beijng, S = Beijing, C = CN";
            objDN.Encode(subjectName, X500NameFlags.XCN_CERT_NAME_STR_NONE);
            objPkcs10.Subject = objDN;

            var objEnroll = new CX509Enrollment();
            objEnroll.InitializeFromRequest(objPkcs10);
            string strRequest = objEnroll.CreateRequest(EncodingType.XCN_CRYPT_STRING_BASE64);

            return strRequest;
        }

        /// <summary>
        /// 将证书请求发送到证书服务
        /// </summary>
        /// <param name="message">证书请求字符串</param>
        /// <returns>证书请求ID，与证书服务上的ID一致。</returns>
        private static int SendCertificateRequest(string message)
        {
            var objCertRequest = new CCertRequest();
            int iDisposition = objCertRequest.Submit(
                CR_IN_BASE64 | CR_IN_FORMATANY,
                message,
                string.Empty,
                @"10.207.230.5\SERVER1-CA");

            switch (iDisposition)
            {
                case CR_DISP_ISSUED:
                    Console.WriteLine("The certificate had been issued.");
                    break;
                case CR_DISP_UNDER_SUBMISSION:
                    Console.WriteLine("The certificate is still pending.");
                    break;
                default:
                    Console.WriteLine("The submission failed: " + objCertRequest.GetDispositionMessage());
                    Console.WriteLine("Last status: " + objCertRequest.GetLastStatus());
                    break;
            }
            return objCertRequest.GetRequestId();
        }

        /// <summary>
        /// 下载并安装证书
        /// </summary>
        /// <param name="requestId"></param>
        private static void DownloadAndInstallCert(int requestId)
        {
            var objCertRequest = new CCertRequest();
            int iDisposition = objCertRequest.RetrievePending(requestId, @"10.207.230.5\SERVER1-CA");

            if (iDisposition == CR_DISP_ISSUED)
            {
                string cert = objCertRequest.GetCertificate(CR_OUT_BASE64 | CR_OUT_CHAIN);
                var objEnroll = new CX509Enrollment();
                objEnroll.Initialize(X509CertificateEnrollmentContext.ContextUser);
                objEnroll.InstallResponse(
                    InstallResponseRestrictionFlags.AllowUntrustedRoot,
                    cert,
                    EncodingType.XCN_CRYPT_STRING_BASE64,
                    null);
                Console.WriteLine("The certificate had been installed successfully.");
            }
        }

        private static void Main(string[] args)
        {
            Console.WriteLine("Request a new certificate? (y|n)");
            if (Console.ReadLine() == "y")
            {
                string request = CreateCertRequestMessage();
                int id = SendCertificateRequest(request);
                Console.WriteLine("Request ID: " + id);
            }

            Console.WriteLine("Download & install certificate? (y|n)");
            if (Console.ReadLine() == "y")
            {
                Console.WriteLine("Request ID?");
                int id = int.Parse(Console.ReadLine());
                DownloadAndInstallCert(id);
            }
        }
    }
}