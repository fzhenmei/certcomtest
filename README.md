这是证书申请的示例代码
======================
1. 创建证书申请请件
2. 发送请求给CA服务
3. 等CA服务通过申请，从服务端获取证书的Response，将证书安装到客户端。   

更多信息访问这里[http://geekswithblogs.net/shaunxu/archive/2012/01/13/working-with-active-directory-certificate-service-via-c.aspx]
